package com.example.countdowntimer

import android.os.Bundle
import android.os.CountDownTimer
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.countdowntimer.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        binding.buttonBasla.setOnClickListener {
            val sayici = object : CountDownTimer(30000,1000){
                override fun onTick(millisUntilFinished: Long) {
                    binding.textViewCikti.text = "Kalan süre : ${millisUntilFinished / 1000} sn !"
                }

                override fun onFinish() {
                    binding.textViewCikti.text = "Bitti!"
                }

            }
            sayici.start()
        }
    }
}